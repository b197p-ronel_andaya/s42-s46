
const Product = require("../models/product.model");
const isAdmin = require("../utils/check-if-admin");
const auth = require('../auth');
async function createProduct(req, res, next) {
  if (!isAdmin(req)) {
    return res
      .status(401)
      .send(
        "You are not an admin. You are not authorized to create a product."
      );
  }
  // if(req.body.quantity < 1){
  //   return res.send('Please input quantity atleast 1');
  // }
  try {
    const newProduct = new Product({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
      img_path: req.file.path
    });
    result = await newProduct.save();
    res.send(`Product ${req.body.name} has been created`);
  } catch (error) {
    return next(error);
  }
}

async function getAllProducts(_, res, next) {
  try {
    const products = await Product.find({ is_active: true });
    if(products.length === 0){
      return res.send('No product listed.');
    }
    return res.send(products);
  } catch (error) {
    return next(error);
  }
}

async function getProduct(req, res, next) {
  try {
    const product = await Product.findById(req.params.id);
    if(!product){
      return res.send(`Product don't exist`);
    }
    return res.send(product);
  } catch (error) {
    return next(error);
  }
}

async function updateProduct(req, res, next) {
  if (!isAdmin(req)) {
    return res
      .status(401)
      .send(
        "You are not an admin. You are not authorized to update this product."
      );
  }
  try {
    const product = await Product.findByIdAndUpdate(req.body.productId, {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
      img_path: req.file.path,
      updated_on: new Date(),
      is_active: req.body.isActive,
    });
    await product.save();
    return res.send("Product Updated");
  } catch (error) {
    return next(error);
  }
}

async function deleteProduct(req, res, next) {
  if (!isAdmin(req)) {
    return res
      .status(401)
      .send(
        "You are not an admin. You are not authorized to delete this product."
      );
  }
  try {
    const product = await Product.findByIdAndUpdate(req.body.productId, {
      is_active: false,
      updated_on: new Date(),
    });
   result = await product.save();
   return res.send("Product Archived");
  } catch (error) {
    return next(error);
  }
}
async function reStock(req,res,next){
  const user = auth.decode(req.headers.authorization);
  try{

    if(!user.isAdmin){
      return res.send('You are not an admin, you are not allowed to re-stock');
    }
    const updateProduct = await Product.findByIdAndUpdate(req.body.productId,{
      quantity: req.body.quantity
    });
    await updateProduct.save();
    res.send(`${updateProduct.name} new quantity is ${req.body.quantity}`); 
  }catch{
    return next(error);
  }
  
}
module.exports = {
  createProduct: createProduct,
  getAllProducts: getAllProducts,
  getProduct: getProduct,
  updateProduct: updateProduct,
  deleteProduct: deleteProduct,
  reStock: reStock
};
