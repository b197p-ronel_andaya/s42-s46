function getTotal(req) {
  let totalAmount = 0;
  req.body.products.map(
    (product) => (totalAmount += product.price * product.quantity)
  );
  return totalAmount;
}

function getProducts(req) {
  const products = [];
  for (const product of req.body.products) {
    products.push({
      product_id: product.productId,
      price: product.price,
      quantity: product.quantity,
      amount: product.quantity * product.price,
      img_path: product.imgPath
    });
  }
  return products;
}

module.exports = {
  getTotal: getTotal,
  getProducts: getProducts,
};
