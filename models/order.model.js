const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  user_id: {
    type: String,
    required: [true, "User ID required"],
  },
  products: [
    {
      product_id: {
        type: String,
        required: [true, "Product ID required"],
      },
      price: {
        type: Number,
        default: 0.00,
      },
      quantity: {
        type: Number,
        default: 0,
      },
      amount:{
        type: Number,
        default: 0.00
      },
      img_path:{
        type: String,
        default: null
      }
    },
  ],
  total_amount:{
    type: Number,
    default: 0.00
  },
  purchased_on:{
    type: Date,
    default: new Date()
  },
  is_cancelled:{
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model("Order", schema);
