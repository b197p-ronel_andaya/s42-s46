const express = require("express");
const multer = require('multer');
const storageConfig = multer.diskStorage({
    destination: (req,file,callback)=>{
        callback(null, 'images/products')  //parameters(error, path)
    },
    filename: (req,file,callback)=>{
        callback(null, Date.now() +'-'+ file.originalname); //parameters(error, filename)
    }
})
const upload = multer({
    storage: storageConfig
});
const router = express.Router();
const productController = require("../controllers/product.controller");
const auth = require("../auth");

router.post("/", auth.verify,upload.single('image'), productController.createProduct);
router.get("/", productController.getAllProducts);
router.get("/:id", productController.getProduct);
router.put("/update", auth.verify,upload.single('image'), productController.updateProduct);
router.patch("/delete", auth.verify, productController.deleteProduct);
router.post("/re-stock", auth.verify, productController.reStock);

module.exports = router;
