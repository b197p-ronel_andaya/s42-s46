const Order = require("../models/order.model");
const Product = require('../models/product.model');
const auth = require("../auth");
const ordersUtil = require("../utils/orders-util");
isAdmin = require("../utils/check-if-admin");

async function createOrder(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  if(user.isAdmin){
    return res.send(
      "This account was made for data administration only, please login to your normal account instead"
    );
  }
  try {
    let totalAmount = ordersUtil.getTotal(req);
    let products = ordersUtil.getProducts(req);

    //manage product quantity
    for (const product of products) {
      const storeProduct = await Product.findById(product.product_id);
      //return if store product quantity is 0
      if (storeProduct.quantity === 0) {
        return res.send(
          `Other users already checked-out this item ${storeProduct.name}. Zero stocks at the moment. Please visit again for new stocks soon. Checkout will not proceed`
        );
      }
      //return if someone already checked out tue product
      if (storeProduct.quantity < product.quantity) {
        return res.send(
          `Other users already checked-out this item ${storeProduct.name}. Only ${storeProduct.quantity} left in the inventory.Please adjust your cart quantity to proceed with the order.`
        );
      }

      storeProduct.quantity -= product.quantity;
      if (storeProduct.quantity <= 0) {
        storeProduct.quantity = 0;
      }
      await storeProduct.save();
    }
    //create new order
    const order = new Order({
      user_id: user.id,
      products: products,
      total_amount: totalAmount,
    });
    await order.save();
    res.send("Order sent!");
  } catch (error) {
    next(error);
  }
}

async function getOrders(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (isAdmin(req)) {
      const orders = await Order.find({});
      return res.send(orders);
    }
    const orders = await Order.find({ user_id: user.id });
    res.send(orders);
  } catch (error) {
    next(error);
  }
}

async function getOrder(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (isAdmin(req)) {
      const order = await Order.findById(req.params.orderId);
      return res.send(order);
    }
    const order = await Order.find({
      _id: req.params.orderId,
      user_id: user.id,
    });
    if (order.length === 0) {
      return res.send("you have no order for that id");
    }
    res.send(order);
  } catch (error) {
    next(error);
  }
}

async function cancelOrder(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (isAdmin(req)) {
      await Order.findByIdAndUpdate(req.body.orderId, {
        is_cancelled: true,
      });
      return res.send("Order Cancelled");
    }

    const result = await Order.updateOne(
      { _id: req.body.orderId, user_id: user.id },
      {
        is_cancelled: true,
      }
    );
    if (result.matchedCount === 0) {
      return res.send("You have no order with that id");
    }
    res.send("Order Cancelled");
  } catch (error) {
    next(error);
  }
}

module.exports = {
  createOrder: createOrder,
  getOrders: getOrders,
  getOrder: getOrder,
  cancelOrder: cancelOrder,
};
