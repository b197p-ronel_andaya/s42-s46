const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product name required"],
  },
  description: {
    type: String,
    required: [true, "Product description required"],
  },
  price: {
    type: Number,
    default: 0.00,
  },
  quantity:{
    type: Number,
    default: 0
  },
  img_path: {
    type: String,
    default: null,
  },
  is_active: {
    type: Boolean,
    default: true,
  },
  created_on: {
    type: Date,
    default: new Date(),
  },
  updated_on: {
    type: Date,
    default: null,
  },
});

module.exports = mongoose.model("Product", schema);
