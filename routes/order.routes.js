const express = require('express');
const router = express.Router();

const auth = require('../auth');

const orderController = require('../controllers/order.controller');

router.post('/' ,auth.verify ,orderController.createOrder);
router.get('/', auth.verify, orderController.getOrders);
router.get('/:orderId',auth.verify,orderController.getOrder);
router.post('/cancelled',auth.verify,orderController.cancelOrder);
module.exports = router

