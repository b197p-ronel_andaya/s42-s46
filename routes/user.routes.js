const express = require('express');
const router = express.Router();
const auth = require('../auth');

const userController = require('../controllers/user.controller');

router.post('/register', userController.register);
router.post('/login', userController.login);
router.put('/access', userController.setUserAccess);
router.get('/details',auth.verify, userController.getUsersDetails);
router.post('/details',auth.verify,userController.getUserDetail);
router.post('/change-password', auth.verify, userController.changePassword);
router.post('/request-password-reset', userController.passwordReset);


module.exports = router;