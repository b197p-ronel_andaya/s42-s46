function errorHandler(error,req,res,next){
    console.log('you are here at error handling middleware');
    console.log(error);
    res.status(500).send('General server error');
}

module.exports = errorHandler;