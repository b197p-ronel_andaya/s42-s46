const express = require('express');
const router = express.Router();
const auth = require('../auth');

const cartController = require('../controllers/cart.controller');

router.post('/',auth.verify, cartController.addProductToCart);
router.get('/viewAll',auth.verify, cartController.viewCart);
router.post('/checkOut', auth.verify,cartController.checkOut);
router.post('/remove', auth.verify, cartController.removeProductToCart);
router.post('/subtract',auth.verify,cartController.subractQuantityToProduct);


module.exports = router;