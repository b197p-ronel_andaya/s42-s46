const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const isAdmin = require("../utils/check-if-admin");
const crypto = require("crypto");

async function register(req, res) {
  const hashedPassword = bcrypt.hashSync(req.body.password, 10);
  const newUser = new User({
    email: req.body.email,
    password: hashedPassword,
  });

  try {
    const user = await User.findOne({ email: req.body.email });
    if (user !== null) {
      return res.send(
        "A user with that email already exist. Please login instead"
      );
    }
    await newUser.save();
    res.send(`User with email ${req.body.email} has been created.`);
  } catch (error) {
    next(error);
  }
}

async function login(req, res) {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user == null) {
      return res.send(false);
    }
    const isPasswordCorrect = bcrypt.compareSync(
      req.body.password,
      user.password
    );
    if (isPasswordCorrect) {
      return res.send({ access: auth.createAccessToken(user) });
    }
    res.send("Username or password incorrect!");
  } catch (error) {
    next(error);
  }
}

async function setUserAccess(req, res, next) {
  if (!isAdmin(req)) {
    return res
      .status(401)
      .send(
        "You are not an admin. You are not authorized to modify a user account."
      );
  }
  try {
    await User.findByIdAndUpdate(req.body.userId, {
      is_admin: req.body.isAdmin,
    });
    res.send("User Access has been updated");
  } catch {
    next(error);
  }
}

async function getUsersDetails(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (isAdmin(req)) {
      const users = await User.find(
        {},
        {
          password: 0,
        }
      );
      return res.send(users);
    }
    const users = await User.findById(user.id, {
      password: 0,
    });
    res.send(users);
  } catch (error) {
    next(error);
  }
}

async function getUserDetail(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (isAdmin(req)) {
      const user = await User.findById(req.body.userId, {
        password: 0,
      });
      return res.send(user);
    }

    if (req.body.userId !== user.id) {
      return res.status(401).send("Not Authorized!");
    }
    const loggedinUser = await User.findById(req.body.userId, {
      password: 0,
    });
    return res.send(loggedinUser);
  } catch (error) {
    next(error);
  }
}

async function changePassword(req, res, next) {
  const user = auth.decode(req.headers.authorization);
  try {
    if (req.body.newPassword !== req.body.confirmNewPassword) {
      return res.send(`New password doesn't match please double check`);
    }

    const userToUpdate = await User.findById(user.id);
    const isPasswordCorrect = bcrypt.compareSync(
      req.body.password,
      userToUpdate.password
    );

    if (!isPasswordCorrect) {
      return res.send("Old password incorrect!Please check");
    }

    const hashedPassword = bcrypt.hashSync(req.body.newPassword, 10);
    userToUpdate.password = hashedPassword;
    const updatedUser = await userToUpdate.save();

    return res.send({ access: auth.createAccessToken(updatedUser) });
  } catch (error) {
    return res.send(error);
  }
}

async function passwordReset(req,res,next){
  const email = req.body.email;

  const password = crypto.randomBytes(3).toString('hex');
  const hashedPassword = bcrypt.hashSync(password,10);
  
  try{
    const user = await User.findOne({email: email});
    user.password = hashedPassword;
    await user.save();
    return res.send(`Your temporary password is ${password}. Please change on login`);
  }catch(error){
    return next(error);
  }
}

module.exports = {
  register: register,
  login: login,
  setUserAccess: setUserAccess,
  getUsersDetails: getUsersDetails,
  getUserDetail: getUserDetail,
  changePassword: changePassword,
  passwordReset: passwordReset
};
