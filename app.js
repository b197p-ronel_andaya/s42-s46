const express = require("express");
const port = process.env.PORT || 4000;
const app = express();

const db = require("./data/database");
db.on("error", () => console.error("Connection Error!"));
db.once("open", () => console.log("Connected to Mongo DB"));

const userRoutes = require("./routes/user.routes");
const productsRoutes = require("./routes/product.routes");
const orderRoutes = require("./routes/order.routes");
const cartRoutes = require("./routes/cart.routes");

const errorHandlerMiddleware = require("./middlewares/error-handling");
const notFoundMiddleware = require("./middlewares/not-found");

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use("/users", userRoutes);
app.use("/products", productsRoutes);
app.use("/orders", orderRoutes);
app.use("/cart", cartRoutes);

app.use(notFoundMiddleware);
app.use(errorHandlerMiddleware);

app.listen(port, () => {
  console.log(`server is now active on port: ${port}`);
});
